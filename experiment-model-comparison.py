import numpy as np
import os
from predict import ImagePredictor
import imageio

from utils.metric import closest_pair_removing
from utils.unet import *


def calculate_average_array(array_of_metrics, prefix):
    precisions = [d["precision"] for d in array_of_metrics]
    recalls = [d["recall"] for d in array_of_metrics]
    f1_scores = [d["f1_score"] for d in array_of_metrics]

    # Calculate the mean of precision, recall, and F1 score
    mean_precision = np.mean(precisions)
    mean_recall = np.mean(recalls)
    mean_f1_score = np.mean(f1_scores)

    std_precision = np.std(precisions)
    std_recall = np.std(recalls)
    std_f1 = np.std(f1_scores)

    # Printing the formatted values
    print(
        f"Results for {prefix}: Precision: {mean_precision:.2f} ± {std_precision:.2f}, Recall: {mean_recall:.2f} ± {std_recall:.2f}, F1: {mean_f1_score:.2f} ± {std_f1:.2f}")


def get_score_dicts(data_path, predictor, eccentricity_range):
    manual_grader_dict = {}
    ao_detect_score_array = []

    # Iterate through subdirectories
    for subdir in os.listdir(data_path):

        # Split the filename on underscores
        parts = subdir.split('_')
        x_value = float(parts[2][1:4]) * (-1 if parts[2][-1] != "T" else 1)

        # Check if the value is in the eccentricity range
        if x_value < eccentricity_range[0] or x_value > eccentricity_range[1]:
            continue

        # Check if subdirectory actually exists
        subdir_path = os.path.join(data_path, subdir)
        if not os.path.isdir(subdir_path):
            continue

        # Get all paths
        grader_paths = [os.path.join(subdir_path, f"annotation_grader_{i}.tiff") for i in range(1, 5)]
        ao_detect_path = os.path.join(subdir_path, "annotation_AO_detect.tiff")
        image_patches_name = [path for path in os.listdir(subdir_path) if os.path.basename(path).startswith("patches")]
        image_patches_path = os.path.join(subdir_path, image_patches_name[0])

        # Load all tiff images as arrays
        grader_arrays = [np.array(imageio.mimread(path)) for path in grader_paths]
        ao_detect_array = np.array(imageio.mimread(ao_detect_path))
        image_patches = np.array(imageio.mimread(image_patches_path))

        # Create the model predictions of the images
        model_prediction = predictor.predict_numpy_array(numpy_array=image_patches)

        # Iterate over all patches in the TIFF file
        for patch_idx, model_patch in enumerate(model_prediction):

            ao_detect_score = closest_pair_removing(model_patch, ao_detect_array[patch_idx], radius=2.4)
            ao_detect_score_array.append(ao_detect_score)

            for grader, grader_array in enumerate(grader_arrays):

                # Calculate the score of the model predictions compared to the each grader array.
                manual_annotation_score = closest_pair_removing(model_patch, grader_array[patch_idx], radius=2.4)

                if grader + 1 not in manual_grader_dict:
                    manual_grader_dict[grader + 1] = []

                manual_grader_dict[grader + 1].append(manual_annotation_score)

    return manual_grader_dict, ao_detect_score_array


if __name__ == "__main__":

    test_dataset_path = "data/test dataset"

    architecture = UNet_5_layers(1, 1, 32)
    checkpoint_path = "utils/model/epoch=47-mean_f1=0.935-mean_precision=0.933-mean_recall=0.939.ckpt"

    predictor = ImagePredictor(architecture=architecture, checkpoint_path=checkpoint_path)



    manual_grader_dict, ao_detect_array = get_score_dicts(data_path=test_dataset_path,
                                                          predictor=predictor,
                                                          eccentricity_range=(2, 10))

    print("")
    print("--------------------")

    print("--------------------")
    # AO detect calculations
    calculate_average_array(ao_detect_array, prefix="AO-detect")

    # Manual grader calculations
    for grader, scores_array in manual_grader_dict.items():
        calculate_average_array(scores_array, prefix=f"{grader}")
