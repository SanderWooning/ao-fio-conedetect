import numpy as np
import os
import imageio
from scipy.spatial import KDTree
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

from utils.metric import closest_pair_removing
from utils.unet import *
from utils.visualizer import plot_model_comparison
from predict import ImagePredictor
import imageio



def calc_array_density_patch(array):
    # Count non-zero values
    non_zero_count = np.count_nonzero(array)

    # Calculate the area of the cropped array in square millimeters
    array_area_mm2 = (98 * 98) / (1000 ** 2)  # Convert microns to millimeters

    # Calculate cell density (cells/mm²)
    cell_density = non_zero_count / array_area_mm2

    return cell_density

def calculate_average_array(array_of_metrics, prefix):
    precisions = [d["precision"] for d in array_of_metrics]
    recalls = [d["recall"] for d in array_of_metrics]
    f1_scores = [d["f1_score"] for d in array_of_metrics]

    # Calculate the mean of precision, recall, and F1 score
    mean_precision = np.mean(precisions)
    mean_recall = np.mean(recalls)
    mean_f1_score = np.mean(f1_scores)

    std_precision = np.std(precisions)
    std_recall = np.std(recalls)
    std_f1 = np.std(f1_scores)

    # Printing the formatted values
    print(
        f"Results for {prefix}: Precision: {mean_precision:.2f} ± {std_precision:.2f}, Recall: {mean_recall:.2f} ± {std_recall:.2f}, F1: {mean_f1_score:.2f} ± {std_f1:.2f}")
    return [[mean_precision, std_precision], [mean_recall, std_recall], [mean_f1_score, std_f1]]

def calc_average_cell(cell_dict):
    """
    Calculate the median coordinate from a dictionary of cell types and their arrays of coordinates.

    Parameters:
    - cell_dict (dict): A dictionary where keys are cell types and values are arrays of coordinates.

    Returns:
    - tuple: The median coordinate calculated based on the x-coordinates of the input coordinates.
    """

    # Initialize an empty list to store all coordinates
    coordinates = []

    # Iterate through the cell types and their arrays of coordinates
    for cell_type, cell_array in cell_dict.items():
        for sublist in cell_array:
            coordinates.append(tuple(sublist[1]))

    # Sort the coordinates based on the x-coordinate
    sorted_coordinates = sorted(coordinates, key=lambda coord: coord[0])

    # Calculate the index of the middle coordinate
    middle_index = len(sorted_coordinates) // 2

    # Check if the number of coordinates is even or odd
    if len(sorted_coordinates) % 2 == 0:
        # If even, calculate the average of the two middle coordinates
        median_x = (sorted_coordinates[middle_index - 1][0] + sorted_coordinates[middle_index][0]) // 2
        median_y = (sorted_coordinates[middle_index - 1][1] + sorted_coordinates[middle_index][1]) // 2
    else:
        # If odd, the median is the middle coordinate
        median_x, median_y = sorted_coordinates[middle_index]

    return (median_x, median_y)


def get_aggregate_score(data_path, predictor, eccentricity_range):
    total_scores_model = []
    total_scores_ao_detect = []

    # Iterate through subdirectories
    for subdir in os.listdir(data_path):

        # Split the filename on underscores
        parts = subdir.split('_')
        x_value = float(parts[2][1:4]) * (-1 if parts[2][-1] != "T" else 1)

        # Check if the value is in the eccentricity range
        if x_value < eccentricity_range[0] or x_value > eccentricity_range[1]:
            continue

        # Check if subdirectory actually exists
        subdir_path = os.path.join(data_path, subdir)
        if not os.path.isdir(subdir_path):
            continue

        # Get all paths
        grader_paths = [os.path.join(subdir_path, f"annotation_grader_{i}.tiff") for i in range(1, 5)]
        ao_detect_path = os.path.join(subdir_path, "annotation_AO_detect.tiff")
        image_patches_name = [path for path in os.listdir(subdir_path) if os.path.basename(path).startswith("patches")]
        image_patches_path = os.path.join(subdir_path, image_patches_name[0])

        # Load all tiff images as arrays
        grader_arrays = [np.array(imageio.mimread(path)) for path in grader_paths]
        ao_detect_array = np.array(imageio.mimread(ao_detect_path))
        image_patches = np.array(imageio.mimread(image_patches_path))

        aggregate_arrays = []

        for patch_idx in range(3):

            # Get patches
            grader_patches = [patch[patch_idx] for patch in grader_arrays]

            # Extract coordinates and find the max annotated grader
            patch_coordinates = [np.argwhere(patch) for patch in grader_patches]
            max_annotated = np.argmax([len(coords) for coords in patch_coordinates])

            # Get the starting aggregate
            starting_aggregate = grader_patches[max_annotated]
            starting_aggregate_coordinates = np.argwhere(starting_aggregate)

            kd_trees = [KDTree(coordinates) for coordinates in patch_coordinates]

            # Create aggregate zero-array to later fill
            aggregate_array = np.zeros_like(grader_patches[0])

            for coordinate in starting_aggregate_coordinates:

                closest_coordinates = {1: [], 2: []}

                for grader, kd_tree in enumerate(kd_trees):

                    grader_array_coordinates = np.argwhere(grader_patches[grader])
                    distance, nearest_point_idx = kd_tree.query(coordinate, k=1)

                    # If the distance it too large
                    if distance > 5:
                        continue

                    closest_coordinate = grader_array_coordinates[nearest_point_idx]
                    row_index, column_index = closest_coordinate

                    value_closest_coordinate = grader_patches[grader][row_index][column_index]

                    closest_coordinates[value_closest_coordinate].append([distance, closest_coordinate])

                # Calculate the average distance
                if len(closest_coordinates[1]) >= 2 or len(closest_coordinates[2]) >= 3 or (
                        len(closest_coordinates[1]) >= 1 and len(closest_coordinates[2]) >= 2):
                    avg_coordinate = calc_average_cell(closest_coordinates)
                    avg_row, avg_column = avg_coordinate
                    aggregate_array[avg_row, avg_column] = 1

            print(f"{subdir}: {calc_array_density_patch(aggregate_array)}")

            # Predict on patch stack
            prediction = predictor.predict_numpy_array(image_patches[patch_idx])

            score_dict_model = closest_pair_removing(prediction, aggregate_array, radius=2.4)
            score_dict_ao_detect = closest_pair_removing(ao_detect_array[patch_idx], aggregate_array, radius=2.4)

            total_scores_model.append(score_dict_model)
            total_scores_ao_detect.append(score_dict_ao_detect)
            aggregate_arrays.append(aggregate_array)

        image_aggr_stack = np.stack(aggregate_arrays)
        # Save the image patches as a TIFF stack
        #output_tiff_path = os.path.join(data_path, subdir, f"aggretate_annotation.tiff")
        #print(output_tiff_path)
        #imageio.mimwrite(output_tiff_path, image_aggr_stack)

    return total_scores_model, total_scores_ao_detect

if __name__ == "__main__":
    test_dataset_path = "data/test dataset"

    architecture = UNet_5_layers(1, 1, 32)
    checkpoint_path = "utils/model/epoch=47-mean_f1=0.935-mean_precision=0.933-mean_recall=0.939.ckpt"

    predictor = ImagePredictor(architecture=architecture, checkpoint_path=checkpoint_path)

    eccenctricity_ranges = [(-2, 2), (3, 6), (7, 10)]

    result_dict_ao_seg = {}
    result_dict_ao_detect = {}

    for ecc in eccenctricity_ranges:
        print(f"Eccentricty: {ecc}")
        total_scores_model, total_scores_ao_detect = get_aggregate_score(data_path=test_dataset_path,
                                                                         predictor=predictor,
                                                                         eccentricity_range=ecc)

        print(total_scores_model)
        print(total_scores_ao_detect)

        ao_seg_results = calculate_average_array(total_scores_model, prefix="AO-FIO ConeDetect")
        ao_detect_results = calculate_average_array(total_scores_ao_detect, prefix="AOdetect")


        t_or_n = "T"
        val = 1
        if ecc[0] < 0:
            t_or_n = "N"
            val = -1

        result_dict_ao_seg[f"{val * ecc[0]}°{t_or_n} - {ecc[1]}°T"] = ao_seg_results
        result_dict_ao_detect[f"{val * ecc[0]}°{t_or_n} - {ecc[1]}°T"] = ao_detect_results


    plot_model_comparison(result_dict_ao_seg, result_dict_ao_detect)