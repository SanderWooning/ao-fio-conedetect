This dataset will be filled using the following structure

training dataset
│
├── AOVHxxx
│   ├── images
│   │   ├── 0.tiff
│   │   ├── 1.tiff
│   │   ├── ...
│   │   └── X.tiff
│   │
│   └── masks
│       ├── 0.tiff
│       ├── 1.tiff
│       ├── ...
│       └── X.tiff
│
├── AOVHxxx
│   ├── images
│   │   ├── 0.tiff
│   │   ├── 1.tiff
│   │   ├── ...
│   │   └── 70.tiff
│   │
│   └── masks
│       ├── 0.tiff
│       ├── 1.tiff
│       ├── ...
│       └── X.tiff
│
│
├── ...
│
└── AOVHxxx
    ├── images
    │   ├── 0.tiff
    │   ├── 1.tiff
    │   ├── ...
    │   └── X.tiff
    │
    └── masks
        ├── 0.tiff
        ├── 1.tiff
        ├── ...
        └── X.tiff