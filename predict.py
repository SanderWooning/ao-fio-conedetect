
import numpy as np
import torch
from PIL import Image
import time

from utils.patch_creator import ImageSlicer
from utils.postprocessing import postprocess, normalize_image
from utils.unet import UNet_5_layers


class ImagePredictor:
    def __init__(self, checkpoint_path, architecture):
        """
        Initializes the ImagePredictor class.

        :param checkpoint_path: Path to the model checkpoint file (str).
        :param architecture: Model architecture to be used.
        """
        self.checkpoint_file_path = checkpoint_path
        self.architecture = architecture

        # Initialize the model without using LitTrainAO
        self.model = self.architecture
        self.load_model_from_checkpoint()

    def load_model_from_checkpoint(self):
        """
        Loads the model from the specified checkpoint file.
        """
        checkpoint = torch.load(self.checkpoint_file_path, map_location=torch.device("cpu"))

        # Handle key mismatch by renaming keys in the state_dict
        state_dict = checkpoint["state_dict"]
        new_state_dict = {}

        for key, value in state_dict.items():
            new_key = key.replace("model.", "")
            new_state_dict[new_key] = value

        self.model.load_state_dict(new_state_dict)
        self.model.eval()

    def predict_image(self, image):
        """
        Predicts an image by breaking it up into subframes.

        :param image: Image to be predicted (str).
        :param overlap_size: Size of the overlap between subframes (int).
        :param exclude_edge: Number of pixels to exclude from the edge of the image (int).

        :return:
            - image_stack: Original subframes.
            - prediction: Processed prediction.
            - y_startpoints: Y-axis starting points of subframes.
            - x_startpoints: X-axis starting points of subframes.
        """

        # If for some reason the image is color, convert to grayscale
        if len(np.shape(image)) > 2:
            image = image.convert("L")

        image_array = np.array(image)

        slicer = ImageSlicer()

        image_stack, x_startpoints, y_startpoints = slicer.create_subframes(image_array=image_array,
                                                                            subframe_size=256,
                                                                            overlap=20,
                                                                            exclude_edge=0)

        print("Predicting")
        batch_size = 16  # Set your desired batch size
        raw_predictions = []

        start_time = time.time()
        for i in range(0, len(image_stack), batch_size):
            print(i, len(image_stack))
            batch_stack = image_stack[i:i + batch_size]
            subframes_tensor = self.prepare_tensor_from_stack(batch_stack)
            output = self.predict_on_tensor(subframes_tensor)

            raw_prediction = np.array(output[:, 0])
            raw_predictions.extend(raw_prediction)

        end_time = time.time()
        elapsed_time = end_time - start_time
        print(f"Prediction Elapsed time: {elapsed_time} seconds")


        start_time = time.time()
        print("Post processing")
        prediction = postprocess(pred_array=np.array(raw_predictions), image_array=image_stack)

        end_time = time.time()
        elapsed_time = end_time - start_time
        print(f"Post-processing Elapsed time: {elapsed_time} seconds")

        print("Visualizing")
        reconstructed_prediction = slicer.reconstruct_image(image_shape=np.shape(image_array),
                                                            subframes=prediction,
                                                            x_startpoints=x_startpoints,
                                                            y_startpoints=y_startpoints)

        return reconstructed_prediction

    def predict_numpy_array(self, numpy_array):
        """
        Predicts on a NumPy array.

        :param numpy_array: Input NumPy array for prediction.

        :return: Processed prediction.
        """

        org_shape_length = len(np.shape(numpy_array))

        # If single 2D-array
        if org_shape_length == 2:
            numpy_array = np.expand_dims(numpy_array, axis=0)

        subframes_tensor = self.prepare_tensor_from_stack(numpy_array)
        output = self.predict_on_tensor(subframes_tensor)

        raw_prediction = np.array(output[:, 0])
        prediction = postprocess(pred_array=raw_prediction, image_array=numpy_array)

        if org_shape_length == 2:
            prediction = prediction[0]

        return prediction

    def prepare_tensor_from_stack(self, data_stack):
        """
        Converts a data stack to a PyTorch tensor.

        :param data_stack: Input data stack.

        :return: PyTorch tensor.
        """
        normalized_data_stack = normalize_image(data_stack)
        subframes_tensor = torch.tensor(normalized_data_stack, dtype=torch.float32)
        subframes_tensor = subframes_tensor.unsqueeze(1)
        return subframes_tensor

    def predict_on_tensor(self, tensor_data):
        """
        Makes a prediction on a PyTorch tensor.

        :param tensor_data: Input PyTorch tensor.

        :return: Model prediction.
        """
        with torch.no_grad():
            output = self.model(tensor_data)
        return output

if __name__ == "__main__":
    data = "data/exampleimages/exampleRTX_image.png"
    predictor = ImagePredictor(
        checkpoint_path="utils/model/epoch=47-mean_f1=0.935-mean_precision=0.933-mean_recall=0.939.ckpt",
        architecture=UNet_5_layers(1, 1, 32))

    predictor.predict_image(image=Image.open(data))
