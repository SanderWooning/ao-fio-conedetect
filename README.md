
<div align="center">
  <a href="https://gitlab.com/radiology/eye/ao-vision/ao-fio-conedetect">
    <img src="./data/exampleimages/logo.svg" alt="Logo">
  </a>
<br>
<br>
</div>


![Python](https://img.shields.io/badge/Python-3.7%2B-black) ![PyTorch](https://img.shields.io/badge/PyTorch-1.9%2B-black) ![PyTorch Lightning](https://img.shields.io/badge/PyTorch%20Lightning-1.4%2B-black)


**AO-FIO ConeDetect**, a component of the AO-VISION project, is an automated cone photoreceptor detection algorithm validated on AO-FIO images. 
The pipeline employs a UNET-based neural network architecture trained on 128-by-128-pixel image patches along with their corresponding center-dot annotated masks. 
Subsequently, post-processing is applied, involving thresholding and max-peak extraction to retrieve the maximum intensity peak.



### :clipboard: Pre-requisites
Before you begin, ensure you have installed python (version 3.7.4 or higher) and pip install the following requirements:
```
pip install -r requirements.txt
```

### :file_folder: Dataset
The data is available here: https://dataverse.nl/dataset.xhtml?persistentId=doi:10.34894/2GXEDZ

The image patches are based on full image taken with an adaptive optics flood illumination ophthalmoscope (AO-FIO). The device used is the RTX1 Adaptive Optics Retinal Camera by Imagine Eyes, France. 

The dataset is made available and consists of two partitions, a training and a testing dataset.
The training dataset contains 625 annotated patches (from 20 images from 18 subjects) and the testing dataset contains 54 annotated patches (from 18 images from 18 subjects).
The testing dataset contains annotations from four different graders. Further information about the annotations can be found in paper.

**Data format and how to load**

- **Training:** The training image patches are individual .tiff files with the dimension of (128x128 pixels). Use any tiff-reader you want. 
- **Test:**  The test image patches and annotations are .tiff files with the dimensions of (3x128x128). **NOTE:** Need to be loaded using imageio.mimread()

```python
import imageio
import tifffile


# Training-dataset images
image_training_path = "path/to/data/image.tiff"
image_array = tifffile.imread(image_training_path)

# Test-dataset images
image_test_path = "path/to/data/image.tiff" 
image_array = imageio.mimread(image_test_path) # Not using mimread will lead to wrong loading of the tiff-file
```

### The UNET Model
The deep learning model used is based on the orignal UNET paper. The model can be trained using the data provided in the training dataset or your own annotated data. For training the model, training.py can be used. 
Fill in the hyper-parameters that you want, and run the script. Pre-trained models are also available in the utils/model folder.

Hyper-parameter tuning was performed on the training-set and resulted in the following parameters:
- **Pre-processing Gaussian blurring sigma:** 1.3
- **Starting learning rate:** 0.003
- **UNET-layers:** 5 layers
- **UNET-starting features:** 32 
- **Loss function:** L1-loss

### :arrows_counterclockwise: Rerun experiments
To ensure full reproducibility, all experiments described in the paper can be rerun using the provided experiment scripts.
These are the exact same scripts used to create the figures in the results. Age data has been removed due to privacy reasons and is the only experiment which is not able to be full replicated.