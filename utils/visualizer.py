import numpy as np
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def visualize_overlay(image_array, prediction_mask):

    min_val = np.min(image_array)
    max_val = np.max(image_array)

    normalized_image = (255 * ((image_array - min_val) / (max_val - min_val))).astype(np.uint8)

    original_image = Image.fromarray(normalized_image).convert("RGB")

    # Create a copy of the image to draw on
    overlay = original_image.copy()

    # Get the coordinates of the dots where the prediction is 1
    y, x = np.where(prediction_mask > 0)

    # Draw dots on the overlay image
    draw = ImageDraw.Draw(overlay)
    for xi, yi in zip(x, y):
        draw.point((xi, yi), fill="red")

    return overlay

def plot_model_comparison(ao_seg_results, ao_detect_results):


    plt.figure(figsize=(18, 10))
    plt.rc('axes', axisbelow=True)
    plt.rcParams.update({'font.size': 18})
    plt.yticks(np.arange(0, 1.1, 0.1))
    plt.grid(axis='y', linestyle='--', alpha=0.7, zorder=0)



    # Set up the bar positions
    bar_width = 0.25
    bar_positions_ao_seg = [i for i in range(len(ao_seg_results))]
    bar_positions_detect = [i + bar_width for i in range(len(ao_seg_results), len(ao_seg_results) + len(ao_detect_results))]

    joined_pos = bar_positions_ao_seg + bar_positions_detect

    tick_positions = [tick_pos + bar_width for tick_pos in joined_pos]
    labels = []


    for i, (eccentricity, results) in enumerate(ao_seg_results.items()):
        plt.bar(bar_positions_ao_seg[i] + (0 * bar_width), results[0][0], width=bar_width, yerr=results[0][1], color="#8dd6ef", zorder=1)
        plt.bar(bar_positions_ao_seg[i] + (1 * bar_width), results[1][0], width=bar_width, yerr=results[0][1], color="#008080", zorder=1)
        plt.bar(bar_positions_ao_seg[i] + (2 * bar_width), results[2][0], width=bar_width, yerr=results[0][1], color="#00216d", zorder=1)

        labels += [f"AO-FIO\nConeDetect\n{eccentricity}"]

        # Display values on top of the bars
        for j, value in enumerate(results):
            plt.text(bar_positions_ao_seg[i] + (0 * bar_width) + j * bar_width, value[0] + value[1] + 0.02, f'{value[0]:.2f}', ha='center', va='bottom', fontsize=16)

    for i, (eccentricity, results) in enumerate(ao_detect_results.items()):
        plt.bar(bar_positions_detect[i] + (0 * bar_width), results[0][0], width=bar_width, yerr=results[0][1], color="#8dd6ef", zorder=1)
        plt.bar(bar_positions_detect[i] + (1 * bar_width), results[1][0], width=bar_width, yerr=results[0][1], color="#008080", zorder=1)
        plt.bar(bar_positions_detect[i] + (2 * bar_width), results[2][0], width=bar_width, yerr=results[0][1], color="#00216d", zorder=1)

        labels += [f"AODetect\n{eccentricity}"]

        # Display values on top of the bars
        for j, value in enumerate(results):
            plt.text(bar_positions_detect[i] + (0 * bar_width) + j * bar_width, value[0] + value[1] + 0.03, f'{value[0]:.2f}', ha='center', va='bottom', fontsize=16)



    # Legend
    legend_labels = ['Precision', 'Recall', 'F1-score']
    legend_colors = ["#8dd6ef", "#008080", "#00216d"]

    # Create custom legend handles with lines of the specified colors
    legend_handles = [Line2D([0], [0], marker='s', color='w', label="Precision", markerfacecolor="#8dd6ef", markersize=10),
                      Line2D([0], [0], marker='s', color='w', label="Recall", markerfacecolor="#008080", markersize=10),
                      Line2D([0], [0], marker='s', color='w', label="F1-score", markerfacecolor="#00216d", markersize=10)]

    # Create the legend
    plt.legend(handles=legend_handles, bbox_to_anchor=(0.7, 1.05), ncol=3)

    plt.xticks(tick_positions, labels)
    plt.ylim(0,1.05)
    plt.show()


    plt.show()





if __name__ == "__main__":
    ao_dect = {"2-2": [[0.9,0.01], [0.89,0.04],[0.87,0.1]],
               "4-6": [[0.9,0.01], [0.89,0.04],[0.87,0.1]],
               "7-10": [[0.9,0.01], [0.89,0.04],[0.87,0.1]]}

    plot_model_comparison(ao_dect, ao_dect)


#
#
#
#
# import os
# import shutil
#
# grader_1_path = "E:/Validation/manual_annotation_sem"
# grader_2_path = "E:/Validation/manual_annotation_sander"
# grader_3_path = "E:/Validation/manual_annotation_manon"
# grader_4_path = "E:/Validation/manual_annotation_grader4"
#
# save_path = "E:/Validation/test dataset"
#
#
# for subfolder in os.listdir(grader_1_path):
#
#     grader_1_subfolder_path = os.path.join(grader_1_path, subfolder, "annotation.tiff")
#     grader_2_subfolder_path = os.path.join(grader_2_path, subfolder, "annotation.tiff")
#     grader_3_subfolder_path = os.path.join(grader_3_path, subfolder, "annotation.tiff")
#     grader_4_subfolder_path = os.path.join(grader_4_path, subfolder, "annotation.tiff")
#
#     grader_1_dest = os.path.join(save_path, subfolder, "annotation_grader_1.tiff")
#     grader_2_dest = os.path.join(save_path, subfolder, "annotation_grader_2.tiff")
#     grader_3_dest = os.path.join(save_path, subfolder, "annotation_grader_3.tiff")
#     grader_4_dest = os.path.join(save_path, subfolder, "annotation_grader_4.tiff")
#
#     shutil.copy(grader_1_subfolder_path, grader_1_dest)
#     shutil.copy(grader_2_subfolder_path, grader_2_dest)
#     shutil.copy(grader_3_subfolder_path, grader_3_dest)
#     shutil.copy(grader_4_subfolder_path, grader_4_dest)

