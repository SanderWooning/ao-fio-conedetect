import numpy as np
import matplotlib.pylab as plt
import cv2 as cv
from PIL import Image
import os
#
# def read_dicom(dicom_path, raw_path):
#     ds = dicom.dcmread(image_path)
#     raw_image_array = read_raw_file(raw_image_path)
#
#     raw_pixel_array = ds.pixel_array
#
#     print(ds)
#     print(np.shape(raw_pixel_array))
#
#     # Define the private creator tag for the private group
#     private_creator_tag = (0x5555, 0x0001)
#
#     # Check if the private creator tag exists in the DICOM dataset
#     if private_creator_tag in ds:
#         # Define the private tags you want to extract
#         private_tags_to_extract = [
#             (0x5555, 0x1001),  # transformation factor b1 for each raw image
#             (0x5555, 0x1002),  # transformation factor b2 for each raw image
#             (0x5555, 0x1005),  # OA grades for each raw image
#             (0x5555, 0x1011),  # transformation factor a11 for each raw image
#             (0x5555, 0x1012),  # transformation factor a12 for each raw image
#             (0x5555, 0x1021),  # transformation factor a21 for each raw image
#             (0x5555, 0x1022),  # transformation factor a22 for each raw image
#             (0x5555, 0x0005),  # fixation position axe X
#             (0x5555, 0x0006),  # fixation position axe Y
#             (0x5555, 0x2001),
#             # ROI, rectangle with the maximum number of images per pixel(indexes starting at 1, last coordinate is outside the ROI)
#             (0x5555, 0x2002)  # Number of images per pixel in the ROI
#         ]
#
#         # Create a list to store transformed images
#         transformation_matrices = []
#         tags = []
#
#         # Iterate through the private tags and extract their values
#         for tag in private_tags_to_extract:
#             tags.append(ds[tag].value)
#
#         # Extract transformation factors from the value
#         b1, b2, oa_grades, a11, a12, a21, a22, fix_x, fix_y, roi, roi_pix = tags
#
#         print(fix_x, fix_y)
#         print(roi, roi_pix)
#
#         for x in range(len(b1)):
#             transform_matrix = np.array([[a11[x], a12[x]], [a21[x], a22[x]]])
#
#             transformation_matrices.append(transform_matrix)
#
#         empty_image = np.zeros((1500, 1500), dtype=np.float32)
#
#         for count, image in enumerate(raw_image_array):
#
#             image = ((image - np.min(image)) / (np.max(image) - np.min(image))).astype(np.float32)
#
#             cv.imshow("img", image)
#             cv.waitKey(0)
#             cv.destroyAllWindows()
#
#             rows, cols = np.shape(image)
#             print(np.shape(image))
#
#             transform_matrix = transformation_matrices[count]
#
#             if transform_matrix == np.array([[0, 0],
#                                              [0, 0]]):
#                 continue
#
#             print(transform_matrix)
#             transformed_image = cv.warpAffine(image, transform_matrix, (rows, cols))
#             cv.imshow("img", transformed_image)
#             cv.waitKey(0)
#             cv.destroyAllWindows()
#             break


def read_mpg(mpg_path):
    # Create a VideoCapture object
    cap = cv.VideoCapture(mpg_path)

    # Check if the video file was opened successfully
    if not cap.isOpened():
        print("Error: Could not open video file.")
        exit()

    frame_count = 0
    frames = []

    # Loop to read frames
    while True:
        # Read a frame from the video file
        ret, frame = cap.read()

        # Break the loop if we have reached the end of the video
        if not ret:
            break

        # Process the frame (e.g., display or save it)
        # You can add your own code here to process the frame

        frames.append(frame)

        # Increment the frame count
        frame_count += 1

    return frames


def save_video(frames):
    # Define the output video path
    output_video_path = "files/output_video.mp4"

    # Check if the frames list is not empty
    if len(frames) > 0:
        # Get the dimensions of the frames (assuming all frames have the same dimensions)
        frame_height, frame_width, _ = frames[0].shape

        # Define the codec and create a VideoWriter object
        fourcc = cv.VideoWriter_fourcc(*'mp4v')  # Use mp4v codec for MP4 format
        out = cv.VideoWriter(output_video_path, fourcc, 30.0, (frame_width, frame_height))

        # Write each frame to the video
        for frame in frames:
            out.write(frame)

        # Release the VideoWriter
        out.release()

        print(f"Video saved as '{output_video_path}'")

    else:
        print("No frames to create a video.")

    # Optional: Open the video using the default video player
    if os.path.exists(output_video_path):
        os.system(output_video_path)


# def register_img(image_list):
#     # Create an empty list to store registered images
#     registered_images = []
#
#     # Choose a reference image (e.g., the first image)
#     reference_image = image_list[0]
#
#     for image in image_list:
#
#         sift_detector = cv.SIFT_create()
#         # Find the keypoints and descriptors with SIFT
#         kp1, des1 = sift_detector.detectAndCompute(reference_image, None)
#         kp2, des2 = sift_detector.detectAndCompute(image, None)
#
#         # BFMatcher with default params
#         bf = cv.BFMatcher()
#         matches = bf.knnMatch(des1, des2, k=2)
#
#         # Filter out poor matches
#         good_matches = []
#         for m, n in matches:
#             if m.distance < 0.75 * n.distance:
#                 good_matches.append(m)
#
#         matches = good_matches
#
#         points1 = np.zeros((len(matches), 2), dtype=np.float32)
#         points2 = np.zeros((len(matches), 2), dtype=np.float32)
#
#         for i, match in enumerate(matches):
#             points1[i, :] = kp1[match.queryIdx].pt
#             points2[i, :] = kp2[match.trainIdx].pt
#
#         # Find homography
#         H, mask = cv.findHomography(points1, points2, cv.RANSAC)
#
#         # Warp image 1 to align with image 2
#         img1Reg = cv.warpPerspective(reference_image, H, (image.shape[1], image.shape[0]))
#
#     return img1Reg


def average_img(registered_images):
    # Convert the list of registered images to a NumPy array
    registered_images = np.array(registered_images)

    # Calculate the average of the registered images
    average_image = np.mean(registered_images, axis=0)

    # Optionally, convert the average_image to uint8 or another data type
    average_image = average_image.astype(np.uint8)

    return average_image


if __name__ == "__main__":
    image_path = "E:/dataset/dicom-files/OD_20231006113200_X2.0N_Y3.0_Z40.0_AOVH045.dcm"
    raw_image_path = "E:/dataset/dicom-files/OD_20231006113200_X2.0N_Y3.0_Z40.0_AOVH045.raw"

    movie_path = "C:/Users/Sander Wooning/Desktop/transfer_2566635_files_a8f08d78/OS_20230921125935_X2.0N_Y-1.0_Z10.0_AOV080.mpg"
    save_path = "C:/Users/Sander Wooning/Desktop/transfer_2566635_files_a8f08d78/Stack"
    # frames = read_mpg(movie_path)

    image_frames = read_mpg(movie_path)

    for i, frame in enumerate(image_frames):

        frame = np.array(frame)
        Image.fromarray(frame).save(f"{save_path}/{i}.tiff")

