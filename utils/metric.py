import numpy as np
from scipy.spatial import KDTree


def closest_pair_removing(pred_mask, annotation_mask, radius):
    """
    Calculates evaluation metrics for the predicted and annotated masks.

    Args:
    pred_mask (ndarray): The predicted mask.
    annotation_mask (ndarray): The annotated mask.
    radius (float): The radius within which to consider a point as a true positive.

    Returns:
    dict: A dictionary containing evaluation metrics including precision, recall, F1 score, and other relevant information.
    """

    # Check if the masks have the same shape and size
    if pred_mask.shape != annotation_mask.shape:
        raise ValueError("Prediction mask and annotation mask must have the same shape.")


    # Find the coordinates of the points in the predicted and annotated masks
    coordinates_pred = np.argwhere(pred_mask)
    coordinates_annotation = np.argwhere(annotation_mask)

    # Convert the coordinates into arrays
    coordinates_pred = np.array(coordinates_pred)
    coordinates_annotation = np.array(coordinates_annotation)

    # Initialize the true positive count
    true_positive_count = 0

    # Build a KDTree for the annotated coordinates for efficient nearest neighbor search
    annotation_tree = KDTree(coordinates_annotation)

    # Iterate through each point in the predicted coordinates
    for point in coordinates_pred:

        # Query the nearest point in the annotations within the radius
        distance, nearest_point_idx = annotation_tree.query(point, k=1)

        # If the distance to the nearest point is within the radius, consider it a true positive
        if distance <= radius:
            true_positive_count += 1

            # Remove the annotated point from the tree
            annotation_tree = KDTree(np.delete(coordinates_annotation, nearest_point_idx, axis=0))


    # Calculate precision and recall
    precision = 0 if len(coordinates_pred) == 0 else true_positive_count / len(coordinates_pred)
    recall = 0 if len(coordinates_annotation) == 0 else true_positive_count / len(coordinates_annotation)

    # Calculate F1 score
    if precision == 0 and recall == 0:
        f1_score = 0
    else:
        f1_score = 2 * (precision * recall) / (precision + recall)

    # Compute additional evaluation metrics
    metric_dict = {
        "number_predicted_cells": len(coordinates_pred),
        "number_annotated_cells": len(coordinates_annotation),
        "precision": precision,
        "recall": recall,
        "f1_score": f1_score,
    }

    return metric_dict


def calculate_average(array_of_metrics):
    precisions = [d["precision"] for d in array_of_metrics]
    recalls = [d["recall"] for d in array_of_metrics]
    f1_scores = [d["f1_score"] for d in array_of_metrics]

    # Calculate the mean of precision, recall, and F1 score
    mean_precision = np.mean(precisions)
    mean_recall = np.mean(recalls)
    mean_f1_score = np.mean(f1_scores)

    std = np.std(f1_scores)

    #Printing the formatted values
    print(f"Mean Precision: {mean_precision:.4f}")
    print(f"Mean Recall: {mean_recall:.4f}")
    print(f"Mean F1 Score: {mean_f1_score:.4f} ± {std:.4f}")
