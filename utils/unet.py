import torch
import torch.nn as nn


class DoubleConvSame(nn.Module):
    def __init__(self, c_in, c_out):
        super(DoubleConvSame, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, kernel_size=3, padding=1),
            nn.BatchNorm2d(c_out),  # Add batch normalization here
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=c_out, out_channels=c_out, kernel_size=3, padding=1),
            nn.BatchNorm2d(c_out),  # Add batch normalization here
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.conv(x)


class UNet_3_layers(nn.Module):
    def __init__(self, c_in, c_out, number_of_features):
        super(UNet_3_layers, self).__init__()

        self.conv1 = DoubleConvSame(c_in=c_in, c_out=number_of_features)
        self.conv2 = DoubleConvSame(c_in=number_of_features, c_out=number_of_features * 2)
        self.conv3 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features * 4)

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        self.up1 = nn.ConvTranspose2d(
            in_channels=number_of_features * 4, out_channels=number_of_features * 2, kernel_size=2, stride=2
        )
        self.up2 = nn.ConvTranspose2d(
            in_channels=number_of_features * 2, out_channels=number_of_features, kernel_size=2, stride=2
        )

        self.up_conv1 = DoubleConvSame(c_in=number_of_features * 4 , c_out=number_of_features * 2)
        self.up_conv2 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features)

        self.conv_1x1 = nn.Conv2d(in_channels=number_of_features, out_channels=c_out, kernel_size=1)

    def forward(self, x):
        """ENCODER"""

        c1 = self.conv1(x)
        p1 = self.pool(c1)

        c2 = self.conv2(p1)
        p2 = self.pool(c2)

        """BOTTLE-NECK"""

        c3 = self.conv3(p2)
        """DECODER"""

        u1 = self.up1(c3)
        cat1 = torch.cat([u1, c2], dim=1)
        uc1 = self.up_conv1(cat1)

        u2 = self.up2(uc1)
        cat2 = torch.cat([u2, c1], dim=1)
        uc2 = self.up_conv2(cat2)

        outputs = self.conv_1x1(uc2)

        return outputs

class UNet_4_layers(nn.Module):
    def __init__(self, c_in, c_out, number_of_features):
        super(UNet_4_layers, self).__init__()

        self.conv1 = DoubleConvSame(c_in=c_in, c_out=number_of_features)
        self.conv2 = DoubleConvSame(c_in=number_of_features, c_out=number_of_features * 2)
        self.conv3 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features * 4)
        self.conv4 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 8)

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        self.up1 = nn.ConvTranspose2d(
            in_channels=number_of_features * 8, out_channels=number_of_features * 4, kernel_size=2, stride=2
        )
        self.up2 = nn.ConvTranspose2d(
            in_channels=number_of_features * 4, out_channels=number_of_features * 2, kernel_size=2, stride=2
        )
        self.up3 = nn.ConvTranspose2d(
            in_channels=number_of_features * 2, out_channels=number_of_features, kernel_size=2, stride=2
        )

        self.up_conv1 = DoubleConvSame(c_in=number_of_features * 8 , c_out=number_of_features * 4)
        self.up_conv2 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 2)
        self.up_conv3 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features)

        self.conv_1x1 = nn.Conv2d(in_channels=number_of_features, out_channels=c_out, kernel_size=1)

    def forward(self, x):
        """ENCODER"""

        c1 = self.conv1(x)
        p1 = self.pool(c1)

        c2 = self.conv2(p1)
        p2 = self.pool(c2)

        c3 = self.conv3(p2)
        p3 = self.pool(c3)

        """BOTTLE-NECK"""

        c4 = self.conv4(p3)
        """DECODER"""

        u1 = self.up1(c4)
        cat1 = torch.cat([u1, c3], dim=1)
        uc1 = self.up_conv1(cat1)

        u2 = self.up2(uc1)
        cat2 = torch.cat([u2, c2], dim=1)
        uc2 = self.up_conv2(cat2)

        u3 = self.up3(uc2)
        cat3 = torch.cat([u3, c1], dim=1)
        uc3 = self.up_conv3(cat3)

        outputs = self.conv_1x1(uc3)

        return outputs

class UNet_5_layers(nn.Module):
    def __init__(self, c_in, c_out, number_of_features):
        super(UNet_5_layers, self).__init__()

        self.conv1 = DoubleConvSame(c_in=c_in, c_out=number_of_features)
        self.conv2 = DoubleConvSame(c_in=number_of_features, c_out=number_of_features * 2)
        self.conv3 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features * 4)
        self.conv4 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 8)
        self.conv5 = DoubleConvSame(c_in=number_of_features * 8, c_out=number_of_features * 16)

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        self.up1 = nn.ConvTranspose2d(
            in_channels=number_of_features * 16, out_channels=number_of_features * 8, kernel_size=2, stride=2
        )
        self.up2 = nn.ConvTranspose2d(
            in_channels=number_of_features * 8, out_channels=number_of_features * 4, kernel_size=2, stride=2
        )
        self.up3 = nn.ConvTranspose2d(
            in_channels=number_of_features * 4, out_channels=number_of_features * 2, kernel_size=2, stride=2
        )
        self.up4 = nn.ConvTranspose2d(
            in_channels=number_of_features * 2, out_channels=number_of_features, kernel_size=2, stride=2
        )

        self.up_conv1 = DoubleConvSame(c_in=number_of_features * 16, c_out=number_of_features * 8)
        self.up_conv2 = DoubleConvSame(c_in=number_of_features * 8 , c_out=number_of_features * 4)
        self.up_conv3 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 2)
        self.up_conv4 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features)

        self.conv_1x1 = nn.Conv2d(in_channels=number_of_features, out_channels=c_out, kernel_size=1)

    def forward(self, x):
        """ENCODER"""

        c1 = self.conv1(x)
        p1 = self.pool(c1)

        c2 = self.conv2(p1)
        p2 = self.pool(c2)

        c3 = self.conv3(p2)
        p3 = self.pool(c3)

        c4 = self.conv4(p3)
        p4 = self.pool(c4)
        """BOTTLE-NECK"""

        c5 = self.conv5(p4)
        """DECODER"""

        u1 = self.up1(c5)
        cat1 = torch.cat([u1, c4], dim=1)
        uc1 = self.up_conv1(cat1)

        u2 = self.up2(uc1)
        cat2 = torch.cat([u2, c3], dim=1)
        uc2 = self.up_conv2(cat2)

        u3 = self.up3(uc2)
        cat3 = torch.cat([u3, c2], dim=1)
        uc3 = self.up_conv3(cat3)

        u4 = self.up4(uc3)
        cat4 = torch.cat([u4, c1], dim=1)
        uc4 = self.up_conv4(cat4)

        outputs = self.conv_1x1(uc4)

        return outputs



class UNet_6_layers(nn.Module):
    def __init__(self, c_in, c_out, number_of_features):
        super(UNet_6_layers, self).__init__()

        self.conv1 = DoubleConvSame(c_in=c_in, c_out=number_of_features)
        self.conv2 = DoubleConvSame(c_in=number_of_features, c_out=number_of_features * 2)
        self.conv3 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features * 4)
        self.conv4 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 8)
        self.conv5 = DoubleConvSame(c_in=number_of_features * 8, c_out=number_of_features * 16)
        self.conv6 = DoubleConvSame(c_in=number_of_features * 16, c_out=number_of_features * 32)

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        self.up1 = nn.ConvTranspose2d(
            in_channels=number_of_features * 32, out_channels=number_of_features * 16, kernel_size=2, stride=2
        )
        self.up2 = nn.ConvTranspose2d(
            in_channels=number_of_features * 16, out_channels=number_of_features * 8, kernel_size=2, stride=2
        )
        self.up3 = nn.ConvTranspose2d(
            in_channels=number_of_features * 8, out_channels=number_of_features * 4, kernel_size=2, stride=2
        )
        self.up4 = nn.ConvTranspose2d(
            in_channels=number_of_features * 4, out_channels=number_of_features * 2, kernel_size=2, stride=2
        )
        self.up5 = nn.ConvTranspose2d(
            in_channels=number_of_features * 2, out_channels=number_of_features, kernel_size=2, stride=2
        )

        self.up_conv1 = DoubleConvSame(c_in=number_of_features * 32, c_out=number_of_features * 16)
        self.up_conv2 = DoubleConvSame(c_in=number_of_features * 16, c_out=number_of_features * 8)
        self.up_conv3 = DoubleConvSame(c_in=number_of_features * 8 , c_out=number_of_features * 4)
        self.up_conv4 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 2)
        self.up_conv5 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features)

        self.conv_1x1 = nn.Conv2d(in_channels=number_of_features, out_channels=c_out, kernel_size=1)

    def forward(self, x):
        """ENCODER"""

        c1 = self.conv1(x)
        p1 = self.pool(c1)

        c2 = self.conv2(p1)
        p2 = self.pool(c2)

        c3 = self.conv3(p2)
        p3 = self.pool(c3)

        c4 = self.conv4(p3)
        p4 = self.pool(c4)

        c5 = self.conv5(p4)
        p5 = self.pool(c5)
        """BOTTLE-NECK"""

        c6 = self.conv6(p5)
        """DECODER"""

        u1 = self.up1(c6)
        cat1 = torch.cat([u1, c5], dim=1)
        uc1 = self.up_conv1(cat1)

        u2 = self.up2(uc1)
        cat2 = torch.cat([u2, c4], dim=1)
        uc2 = self.up_conv2(cat2)

        u3 = self.up3(uc2)
        cat3 = torch.cat([u3, c3], dim=1)
        uc3 = self.up_conv3(cat3)

        u4 = self.up4(uc3)
        cat4 = torch.cat([u4, c2], dim=1)
        uc4 = self.up_conv4(cat4)

        u5 = self.up5(uc4)
        cat5 = torch.cat([u5, c1], dim=1)
        uc5 = self.up_conv5(cat5)

        outputs = self.conv_1x1(uc5)

        return outputs

class UNet_7_layers(nn.Module):
    def __init__(self, c_in, c_out, number_of_features):
        super(UNet_7_layers, self).__init__()

        self.conv1 = DoubleConvSame(c_in=c_in, c_out=number_of_features)
        self.conv2 = DoubleConvSame(c_in=number_of_features, c_out=number_of_features * 2)
        self.conv3 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features * 4)
        self.conv4 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 8)
        self.conv5 = DoubleConvSame(c_in=number_of_features * 8, c_out=number_of_features * 16)
        self.conv6 = DoubleConvSame(c_in=number_of_features * 16, c_out=number_of_features * 32)
        self.conv7 = DoubleConvSame(c_in=number_of_features * 32, c_out=number_of_features * 64)

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        self.up1 = nn.ConvTranspose2d(
            in_channels=number_of_features * 64, out_channels=number_of_features * 32, kernel_size=2, stride=2
        )
        self.up2 = nn.ConvTranspose2d(
            in_channels=number_of_features * 32, out_channels=number_of_features * 16, kernel_size=2, stride=2
        )
        self.up3 = nn.ConvTranspose2d(
            in_channels=number_of_features * 16, out_channels=number_of_features * 8, kernel_size=2, stride=2
        )
        self.up4 = nn.ConvTranspose2d(
            in_channels=number_of_features * 8, out_channels=number_of_features * 4, kernel_size=2, stride=2
        )
        self.up5 = nn.ConvTranspose2d(
            in_channels=number_of_features * 4, out_channels=number_of_features * 2, kernel_size=2, stride=2
        )
        self.up6 = nn.ConvTranspose2d(
            in_channels=number_of_features * 2, out_channels=number_of_features, kernel_size=2, stride=2
        )

        self.up_conv1 = DoubleConvSame(c_in=number_of_features * 64, c_out=number_of_features * 32)
        self.up_conv2 = DoubleConvSame(c_in=number_of_features * 32, c_out=number_of_features * 16)
        self.up_conv3 = DoubleConvSame(c_in=number_of_features * 16, c_out=number_of_features * 8)
        self.up_conv4 = DoubleConvSame(c_in=number_of_features * 8, c_out=number_of_features * 4)
        self.up_conv5 = DoubleConvSame(c_in=number_of_features * 4, c_out=number_of_features * 2)
        self.up_conv6 = DoubleConvSame(c_in=number_of_features * 2, c_out=number_of_features)

        self.conv_1x1 = nn.Conv2d(in_channels=number_of_features, out_channels=c_out, kernel_size=1)

    def forward(self, x):
        """ENCODER"""

        c1 = self.conv1(x)
        p1 = self.pool(c1)

        c2 = self.conv2(p1)
        p2 = self.pool(c2)

        c3 = self.conv3(p2)
        p3 = self.pool(c3)

        c4 = self.conv4(p3)
        p4 = self.pool(c4)

        c5 = self.conv5(p4)
        p5 = self.pool(c5)

        c6 = self.conv6(p5)
        p6 = self.pool(c6)

        """BOTTLE-NECK"""

        c7 = self.conv7(p6)

        """DECODER"""

        u1 = self.up1(c7)
        cat1 = torch.cat([u1, c6], dim=1)
        uc1 = self.up_conv1(cat1)

        u2 = self.up2(uc1)
        cat2 = torch.cat([u2, c5], dim=1)
        uc2 = self.up_conv2(cat2)

        u3 = self.up3(uc2)
        cat3 = torch.cat([u3, c4], dim=1)
        uc3 = self.up_conv3(cat3)

        u4 = self.up4(uc3)
        cat4 = torch.cat([u4, c3], dim=1)
        uc4 = self.up_conv4(cat4)

        u5 = self.up5(uc4)
        cat5 = torch.cat([u5, c2], dim=1)
        uc5 = self.up_conv5(cat5)

        u6 = self.up6(uc5)
        cat6 = torch.cat([u6, c1], dim=1)
        uc6 = self.up_conv6(cat6)

        outputs = self.conv_1x1(uc6)
        return outputs

