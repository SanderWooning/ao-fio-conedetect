import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import kde
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from PIL import Image, ImageFilter,ImageOps
from matplotlib.colors import Normalize

#https://python-graph-gallery.com/86-avoid-overlapping-in-scatterplot-with-2d-density/

def create_density_image(cone_coordinates, image_size_mm, image_size, min_density, max_density):

    coordinate_array = np.zeros(image_size)
    for coord in cone_coordinates.keys():
        coordinate_array[coord[1], coord[0]] = 1

    width, height = image_size

    bin_size = 50  # Set the desired bin size in pixels

    nbins_x = width // bin_size
    nbins_y = height // bin_size

    cone_density = np.zeros((nbins_x, nbins_y))

    bin_area = (image_size_mm[0] / nbins_x) * (image_size_mm[1] / nbins_y)

    for i in range(nbins_x):
        for j in range(nbins_y):
            x_start, x_end = i * bin_size, (i + 1) * bin_size
            y_start, y_end = j * bin_size, (j + 1) * bin_size

            density = np.sum(coordinate_array[x_start:x_end, y_start:y_end]) / bin_area

            cone_density[i, j] = density

    # Normalize the colors based on the specified range
    norm = Normalize(vmin=min_density, vmax=max_density)

    # Set the figure size explicitly
    fig = plt.figure(figsize=(width / 100, height / 100))

    # Create the color mesh plot with the specified colormap and normalization
    plt.pcolormesh(
        np.linspace(0, width, nbins_x),
        np.linspace(0, height, nbins_y),
        cone_density.T,
        shading='gouraud',
        cmap="rainbow",
        norm=norm
    )

    # Remove axis and padding
    plt.axis('off')
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

    # Create a canvas to render the figure
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()

    density_map_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())

    # Resize the density map image to match the original image size
    density_map_image = density_map_image.resize((width, height))
    #density_map_image = ImageOps.flip(density_map_image)
    density_map_image = density_map_image.rotate(-90, Image.NEAREST, expand = 1)

    return density_map_image







def density_map_cone_count():
    cone_path = "C:/Users/Sander/Downloads/coneimage_OS_20230906170917_X0.0N_Y3.0_Z30.0.png"
    original_image = Image.open("C:/Users/Sander/Desktop/OS_20230906170917_X0.0N_Y3.0_Z30.0_AOVH039.png")
    org_image_arr = np.array(original_image)

    normalized_image = (255 * ((org_image_arr - np.min(org_image_arr)) / (np.max(org_image_arr) - np.min(org_image_arr)))).astype(np.uint8)
    original_image = Image.fromarray(normalized_image).convert("RGB")

    cone_mask = np.array(Image.open(cone_path).convert("L"))

    width, height = np.shape(cone_mask)

    nbins = 25
    cone_density = np.zeros((nbins, nbins))

    for i in range(nbins):
        for j in range(nbins):
            # Calculate the region bounds
            x_start, x_end = i * (width // nbins), (i + 1) * (width // nbins)
            y_start, y_end = j * (height // nbins), (j + 1) * (height // nbins)

            # Count the number of cones in the region
            cone_density[i, j] = np.sum(cone_mask[x_start:x_end, y_start:y_end])


    # Set the figure size explicitly
    fig = plt.figure(figsize=(width/100, height/100))

    # Create the color mesh plot without displaying it
    plt.pcolormesh(np.linspace(0, width, nbins), np.linspace(0, height, nbins), cone_density, shading='gouraud', cmap="rainbow")

    # Remove axis and padding
    plt.axis('off')
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

    # Create a canvas to render the figure
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()

    density_map_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())

    # Resize the density map image to match the original image size
    density_map_image = density_map_image.resize((width, height))
    density_map_image = density_map_image.resize((np.shape(original_image)[0:2]))
    #density_map_image = ImageOps.mirror(density_map_image)
    density_map_image = ImageOps.flip(density_map_image)


    # Blend the original image and the density map image with transparency
    overlay_image = Image.blend(original_image, density_map_image, alpha=0.5)

    # Display or save the overlay image as needed
    overlay_image.show()


def density_map_self():
    # Load the image
    cone_path = "C:/Users/Sander/Downloads/coneimage_OS_20230906170917_X0.0N_Y3.0_Z30.0.png"  # Replace with the actual path to your image
    image_array = np.array(Image.open(cone_path).convert("L"))

    # Define the size of each patch
    patch_size = 25

    # Get the dimensions of the image
    height, width = image_array.shape

    # Calculate the number of patches in each dimension
    num_patches_height = height // patch_size
    num_patches_width = width // patch_size

    # Initialize a list to store the densities
    densities = []

    # Iterate over each patch
    for i in range(num_patches_height):
        for j in range(num_patches_width):
            # Define the coordinates of the current patch
            start_row = i * patch_size
            end_row = start_row + patch_size
            start_col = j * patch_size
            end_col = start_col + patch_size

            # Extract the current patch from the image
            patch = image_array[start_row:end_row, start_col:end_col]

            # Calculate the density (non-zero pixel ratio)
            density = np.count_nonzero(patch) / patch.size

            # Append the density to the list
            densities.append(density)

    # Reshape the densities into a 2D array for plotting
    densities_array = np.array(densities).reshape(num_patches_height, num_patches_width)
    min_val = np.min(densities_array)
    max_val = np.max(densities_array)

    plt.imshow(densities_array)
    plt.show()

    # Use the same density normalization as in density_map_kde
    normalized_image = (255 * ((densities_array - min_val) / (max_val - min_val))).astype(np.uint8)

    # Create a similar density map visualization
    fig = plt.figure(figsize=(width/100, height/100))
    xi, yi = np.mgrid[0:width:width*1j, 0:height:height*1j]
    zi = normalized_image.flatten()
    # Correct the reshaping of zi
    # Correct the reshaping of zi
    zi_reshaped = zi.reshape(num_patches_height, num_patches_width)


    plt.pcolormesh(xi, yi, zi_reshaped, shading='gouraud', cmap="rainbow")

    plt.axis('off')
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

    # Create a canvas to render the figure
    canvas = FigureCanvas(fig)
    canvas.draw()

    density_map_image = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())

    # Resize the density map image to match the original image size
    density_map_image = density_map_image.resize((width, height))

    # Display or save the overlay image as needed
    density_map_image.show()
    # Or save the image to a file


    # Create a plot with smooth interpolation
    # plt.imshow(densities_array, cmap='rainbow', interpolation='bilinear', aspect='auto')
    # plt.colorbar(label='Density')
    # plt.title('Density Map of Image Patches with Gradient')
    # plt.xlabel('Patch Width Index')
    # plt.ylabel('Patch Height Index')
    # plt.show()

